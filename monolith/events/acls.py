from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests, json


def get_photo(city, state):
    dic = {"Authorization": PEXELS_API_KEY}
    meter = {
        "per_page": 1,
        "query": f"{city} {state}",
    }

    url = "https://api.pexels.com/ v1/search"
    response = requests.get(
        url,
        params=meter,
        headers=dic,
    )
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    ret_url = "http://api.openweathermap.org/geo/1.0/direct"
    ret_meters = {
        "q": f"{city},{state},us",
        "limit": 5,
        "appid": OPEN_WEATHER_API_KEY,
    }
    ret_res = requests.get(ret_url, params=ret_meters)
    ret_content = json.loads(ret_res.content)
    try:
        lat = ret_content[0]["lat"]
        lon = ret_content[0]["lon"]
    except:
        return None
    url = "https://api.openweathermap.org/data/2.5/weather"
    meters = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "metric",
    }
    res = requests.get(url, params=meters)
    data = res.json()
    description = data["weather"][0]["description"]
    temperature = data["main"]["temp"]
    try:
        return {
            "temprature": temperature,
            "description": description,
        }
    except:
        return None
    # Use the Open Weather API
