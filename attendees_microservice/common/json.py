from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, datetime):
            return object.isoformat()
        else:
            return super().default(object)


class QuerySetEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, QuerySet):
            return list(object)
        else:
            return super().default(object)


class StatusEncoder(JSONEncoder):
    def default(self, object):
        if isinstance(object, Status):
            return object.name
        else:
            return super().default(object)


class ModelEncoder(DateEncoder, QuerySetEncoder, StatusEncoder, JSONEncoder):
    encoders = {}

    def default(self, object):
        if isinstance(object, self.model):
            d = {}
            if hasattr(object, "get_api_url"):
                d["href"] = object.get_api_url()
            for property in self.properties:
                value = getattr(object, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(object))
            return d
        else:
            return super().default(object)

    def get_extra_data(self, object):
        return {}
